var Template = {
    dir: Ti.Filesystem.getResourcesDirectory()+"/tpl/",
    /**
     * Retorna o template com Parse
     * @param {type} file
     * @param {type} arrVars
     * @returns {Template.fetch.content|String}
     */
    fetch: function(file, arrVars) {
        var content = File.read(this.dir+file);
        var value,varname;
        for(varname in arrVars){
            value = arrVars[varname];
            content = str_replace('{'+varname+'}',arrVars[varname],content);
        }
        return content;
    }

}