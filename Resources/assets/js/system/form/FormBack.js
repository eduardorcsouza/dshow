$.FormBack = $.extend($.Form,{
    
    table: 'tbl_config_back',
    pkName: 'id_back',
    config: function(){
        var dataObj = cloneObj($.Form.fields.text);
        dataObj.name = 'dia'
        dataObj.label = 'Dia'
        this.addField(dataObj);
        
        var dataObj = cloneObj($.Form.fields.date);
        dataObj.name = 'data'
        dataObj.label = 'Data'
        this.addField(dataObj);
        
        var imageObj = cloneObj($.Form.fields.image);
        imageObj.name = 'file'
        imageObj.label = 'Imagem'
        this.addField(imageObj);
    }
    
})