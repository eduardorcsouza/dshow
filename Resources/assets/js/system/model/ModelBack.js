function ModelBack() {

    /**
     * retorna o plano de fundo cadastrado
     * @param {type} where
     * @returns {Boolean}
     */
    var _getbackground = function(where) {
        var sql = "SELECT * FROM tbl_config_back WHERE " + where;
        var resultObj = databaseObj.execute(sql);
        if (resultObj) {
            var file = resultObj.fieldByName('file');
            if (file) {
                return file;
            }
        }
        return false;
    }

    /**
     * Retorna o fundo de uma data especifica
     * @returns {Boolean}
     */
    var _byData = function() {
        var curDate = date('d/m/Y');
        return _getbackground("data='" + curDate + "'");
    }

    /**
     * Retorna o fundo de um dia na semana com um numeral
     * @returns {Boolean}
     */
    var _byDayNum = function() {
        var curDate = date('d/m/Y');
        var curDay = weekDayShort(new Date()).toLowerCase();
        var sql = "SELECT * FROM tbl_config_dias WHERE data = '" + curDate + "'";
        var resultObj = databaseObj.execute(sql);
        var day = resultObj.fieldByName('num') + ' ' + curDay;

        return _getbackground("dia like '%" + day + "%'");
    }

    /**
     * Retorna o fundo de um dia da semana
     * @returns {Boolean}
     */
    var _byDiasemana = function() {
        var curDay = weekDayShort(new Date());
        return _getbackground("dia='" + curDay + "'");
    }

    /**
     * Retorna o fundo de um dia da semana
     * @returns {Boolean}
     */
    var _byDefault = function() {
        var curDay = weekDayShort(new Date());
        return _getbackground("(data is null or data = '') AND (dia is null or dia = '')");
    }

    /**
     * Retorna o plano de fundo
     * @returns {Boolean}
     */
    this.getBack = function() {
        var back = _byData();
        if (back === false) {
            back = _byDayNum();
        }
        if (back === false) {
            back = _byDiasemana();
        }
        if (back === false) {
            back = _byDefault();
        }
        return back;
    }

}