$.Populate = {
    /**
     * Popula o combo com dados de um array
     * @param {String|Object} combo
     * @param {Array} arrCombo
     * @param {String} val
     * @param {String} desc
     * @returns {undefined}
     */
    combo: function(combo, arrCombo, val, desc, selected) {
        var optionObj;
        var comboObj = $(combo);
        comboObj.html('');
        for (var i = 0; i < arrCombo.length; i++) {
            optionObj = document.createElement('option');
            if(selected == arrCombo[i][val]){
                $(optionObj).attr("selected",true);
            }
            $(optionObj).val(arrCombo[i][val])
                    .html(arrCombo[i][desc]);
            comboObj.append(optionObj);
        }
    },
    /**
     * Popula um UL
     * @param {string|Object} ul
     * @param {array} arr
     * @param {string} val
     * @param {string} desc
     * @returns {undefined}
     */
    ul: function(ul, arr, val, desc) {
        var liObj;
        var ulObj = $(ul);
        ulObj.html('');
        for (var i = 0; i < arr.length; i++) {
            liObj = document.createElement('li');

            $(liObj).attr('rel', arr[i][val])
                    .html(arr[i][desc]);
            ulObj.append(liObj);
        }
    },
    lister: function(ul, arr, title, desc, val) {
        var liObj;
        var ulObj = $(ul);
        for (var i = 0; i < arr.length; i++) {
            liObj = document.createElement('li');

            $(liObj).attr('rel', arr[i][val])
                    .html('<div class="title">' + arr[i][title] + '</div><div class="desc">' + arr[i][desc] + "</div>");
            ulObj.append(liObj);
        }
        $(ulObj).addClass('lister-box')
    }

}