var File = {
    /**
     * Lê um arquivo e retorna o conteudo
     * @param {string} dir
     * @param {string} file
     * @returns {string}
     */
    read: function(file) {
        var fileOBj = Ti.Filesystem.getFile(file);
        return fileOBj.read().toString();
    },
    /**
     * Grava informacoes no arquivo
     * @param {string} data
     * @returns {undefined}
     */
    write: function(file, data) {
        var fileOBj = Ti.Filesystem.getFile(file);
        fileOBj.write(data);
        fileOBj.close();
    }

}