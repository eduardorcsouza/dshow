function Sqlite(db) {
    var dbObj = Ti.Database.openFile(Ti.Filesystem.getFile(
            Ti.Filesystem.getResourcesDirectory(), db));
    /**
     * Executa comandos de banco
     * @param {string} sql
     * @returns {Ti.Database.Resultset}
     */
    this.execute = function(sql){
        return dbObj.execute(sql);
    };
    
    /**
     * Fecha a conexão do banco de dados
     * @returns {undefined}
     */
    this.close = function(){
        dbObj.close();
    }
}