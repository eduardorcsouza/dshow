function ModelBiblia() {

    /**
     * Retorna um array da biblia
     * @param {type} result
     * @returns {ModelBiblia.getRow.Anonym$0|Boolean}
     */
    var getRow = function(result) {
        if (result.isValidRow()) {

            return {
                id: result.fieldByName('id_versiculo'),
                livro: result.fieldByName('livro'),
                id_livro: result.fieldByName('id_livro'),
                capitulo: result.fieldByName('capitulo'),
                versiculo: result.fieldByName('versiculo'),
                texto: result.fieldByName('texto')
            }
        }
        return false;
    }

    /**
     * Retorna os livros
     * @returns {Array}
     */
    this.getLivros = function() {
        var sql = "SELECT * FROM tbl_biblia_livros";
        var resultObj = databaseObj.execute(sql);
        var arrLivros = [];
        var id, nome;
        while (resultObj.isValidRow()) {
            id = resultObj.fieldByName('id_livro');
            nome = resultObj.fieldByName('nome');
            arrLivros.push({
                id: id,
                nome: nome
            });
            resultObj.next();
        }
        return arrLivros;
    };

    /**
     * Retorna um array com os dados da escritura
     * @param {int} livro
     * @param {int} capitulo
     * @param {int} versiculo
     * @returns {ModelBiblia.getRow.Anonym$0|Boolean}
     */
    this.getEscrituraByReferencia = function(livro, capitulo, versiculo) {
        var sql = "SELECT v.*,l.nome as livro FROM tbl_biblia_versiculos v"
                + " JOIN tbl_biblia_livros l ON l.id_livro=v.id_livro"
                + " where v.id_livro = " + livro + " and v.capitulo = " + capitulo + " and v.versiculo = " + versiculo + ""
                + " and v.id_versao = (SELECT value FROM tbl_config_param WHERE nome='bible_version')";
        var result = databaseObj.execute(sql);
        return getRow(result);
    }

    /**
     * Retorna um array com os dados da escritura conforme o ID
     * @param {int} id
     * @returns {ModelBiblia.getRow.Anonym$0|Boolean}
     */
    this.getEscrituraByID = function(id) {
        if (id <= 0) {
            id = 1;
        }
        var sql = "SELECT v.* FROM tbl_biblia_versiculos v"
                + " where v.id_versiculo = " + id;
        var result = databaseObj.execute(sql);
        return this.getEscrituraByReferencia(
                result.fieldByName('id_livro'),
                result.fieldByName('capitulo'),
                result.fieldByName('versiculo')
                )
    }


    /**
     * Retorna as verssoes da bíblia
     * @returns {ModelBiblia.bibleVersions.arr|Array}
     */
    this.bibleVersions = function() {
        var resultObj = databaseObj.execute('SELECT * FROM tbl_biblia_versoes');
        var arr = [];
        while (resultObj.isValidRow()) {

            arr.push({
                id: resultObj.fieldByName('id_versao'),
                name: resultObj.fieldByName('nome')
            });

            resultObj.next();
        }
        return arr;
    }


    /**
     * Retorna as verssoes da bíblia
     * @returns {ModelBiblia.bibleVersions.arr|Array}
     */
    this.listSalvos = function() {
        var configObj = new ModelConfig();
        var cur = time() + (1000 * 60 * 60 * 5);
        var version = configObj.getConfig('bible_version');
        var sql = "SELECT bs.id_salvo,bl.id_livro, bl.nome as livro, bs.capitulo, bs.versiculo, bv.texto \n\
                    FROM tbl_biblia_salvos bs \n\
                    JOIN tbl_biblia_livros bl ON bl.id_livro=bs.id_livro JOIN tbl_biblia_versiculos bv \n\
                        ON (bv.capitulo=bs.capitulo AND bv.versiculo = bs.versiculo AND bv.id_livro=bs.id_livro) \n\
                    WHERE bv.id_versao='" + version + "' AND bs.expiracao <= " + cur;
        var resultObj = databaseObj.execute(sql);
        var arr = [];
        var livro, capitulo, versiculo;
        while (resultObj.isValidRow()) {

            livro = resultObj.fieldByName('livro');
            capitulo = resultObj.fieldByName('capitulo');
            versiculo = resultObj.fieldByName('versiculo');
            arr.push({
                id_salvo: resultObj.fieldByName('id_salvo'),
                id_livro: resultObj.fieldByName('id_livro'),
                livro: resultObj.fieldByName('livro'),
                capitulo: resultObj.fieldByName('capitulo'),
                versiculo: resultObj.fieldByName('versiculo'),
                texto: resultObj.fieldByName('texto'),
                desc: livro +
                        ' ' + capitulo +
                        ':' + versiculo
            });

            resultObj.next();
        }
        return arr;
    }

    /**
     * Carrega um versiculo salvo
     * @param {type} idSave
     * @returns {ModelBiblia.getRow.Anonym$0|Boolean}
     */
    this.bibleLoadBySave = function(idSave) {
        var cur = time() + (1000 * 60 * 60 * 5);
        var sql = "SELECT * FROM tbl_biblia_salvos WHERE expiracao <= " + cur + " AND id_salvo = " + idSave;
        var resultObj = databaseObj.execute(sql);
        if (resultObj.isValidRow()) {
            return this.getEscrituraByReferencia(
                    resultObj.fieldByName('id_livro'),
                    resultObj.fieldByName('capitulo'),
                    resultObj.fieldByName('versiculo'));
        }
        return false;
    }

    /**
     * Salva um versiculo da biblia
     * @param {type} id
     * @returns {undefined}
     */
    this.bibleSaveVersiculo = function(id) {
        var arrEscritura = this.getEscrituraByID(id);
        var cur = time() + (1000 * 60 * 60 * 5);
        var exists = false;

        var checkSql = "SELECT * FROM tbl_biblia_salvos WHERE ";
        checkSql += "id_livro='" + arrEscritura['id_livro'] + "' ";
        checkSql += "AND capitulo='" + arrEscritura['capitulo'] + "' ";
        checkSql += "AND versiculo='" + arrEscritura['versiculo'] + "' ";
        var resultObj = databaseObj.execute(checkSql);
        if (resultObj.isValidRow()) {
            var expire = parseInt(resultObj.fieldByName('expiracao'));
            if (expire > time(time())) {
                exists = true;
            }
        }

        if (!exists) {
            var sql = "INSERT INTO tbl_biblia_salvos (id_livro, capitulo, versiculo,expiracao) values ";
            sql += "('" + arrEscritura['id_livro'] + "','" + arrEscritura['capitulo'] + "','" + arrEscritura['versiculo'] + "','" + cur + "')";
            databaseObj.execute(sql);
        }
    }

    this.buscar = function(busca) {
        var sqlWord = '';
        var arrSql = [];
        var arrWord = strToWords(busca);
        if (arrWord.length > 0) {
            for (var i = 0; i < arrWord.length; i++) {
                if (arrWord[i].length > 3) {
                    arrWord[i] = str_replace("'", "\\'", arrWord[i]);
                    arrSql.push("v.texto_noacent LIKE '%" + arrWord[i] + "%'");
                }
            }
        }
        if (arrSql.length > 0) {
            sqlWord = arrSql.join(' OR ');
        }

        var sql = "SELECT * FROM (SELECT v.*, l.nome as livro FROM tbl_biblia_versiculos v \n\
            JOIN tbl_biblia_livros l on l.id_livro=v.id_livro \n\
            WHERE v.texto_noacent LIKE '%criou Deus%'\n\
            and v.id_versao = (SELECT value FROM tbl_config_param WHERE nome='bible_version')";
        if (sqlWord !== '') {
            sql += " UNION SELECT v.*, l.nome as livro FROM tbl_biblia_versiculos v \n\
            JOIN tbl_biblia_livros l on l.id_livro=v.id_livro \n\
            WHERE (" + sqlWord + ") and v.id_versao = (SELECT value FROM tbl_config_param WHERE nome='bible_version') ";
        }
        sql += ') LIMIT 1000';
        var resultObj = databaseObj.execute(sql);
        var arr = [];
        while (resultObj.isValidRow()) {

            var texto = resultObj.fieldByName("texto").toString().substring(0, 160)

            arr.push({
                id_versiculo: resultObj.fieldByName("id_versiculo"),
                title: resultObj.fieldByName("livro") + ' ' + resultObj.fieldByName("capitulo"),
                desc: resultObj.fieldByName("versiculo") + ' ' + texto + '...'
            });

            resultObj.next();
        }
        return arr;
    }

}