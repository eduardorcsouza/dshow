function ModelConfig() {

    this.bibleVersions = function() {
        var bible = new ModelBiblia();
        return bible.bibleVersions();
    }
    
    /**
     * Seta um config
     * @param {type} config
     * @param {type} val
     * @returns {undefined}
     */
    this.setConfig = function(config,val){
        var sql = "SELECT * FROM tbl_config_param WHERE nome='"+config+"'";
        var resultObj = databaseObj.execute(sql);
        if(resultObj.isValidRow()){
            var id = resultObj.fieldByName('id_param');
            var update = "UPDATE tbl_config_param SET value='"+val+"' WHERE id_param="+id;
            databaseObj.execute(update);
        }else{
            var insert = "INSERT INTO tbl_config_param (nome,value) values ('"+config+"','"+val+"')";
            databaseObj.execute(insert);
        }
    }
    
    /**
     * Retorna um config
     * @param {type} config
     * @returns {string}
     */
    this.getConfig = function(config){
        var sql = "SELECT * FROM tbl_config_param WHERE nome='"+config+"'";
        var resultObj = databaseObj.execute(sql);
        if(resultObj.isValidRow()){
            return resultObj.fieldByName('value');
        }
        return null
    }

}