function ViewConfig() {
    var obj = this;
    /**
     * Carrega os valores da bíblia no combo
     * @returns {undefined}
     */
    this.loadVersionsBible = function() {
        var modelObj = new ModelConfig();
        $.Populate.combo('#bible-versions', modelObj.bibleVersions(), 'id', 'name', modelObj.getConfig('bible_version'));
        this.triggerBiblia();
    }

    /**
     * Criar o formulario para o fundo
     * @returns {undefined}
     */
    this.CreateformBackView = function() {
        var date = $.fields.date('date', 'Data');
        var charDate = $.fields.text('char-date', 'Data Manual');
        var image = $.fields.image('imagem', 'Imagem');
        var save = $.fields.savebtn('save', 'Salvar');
        var form = '<form class="form-horizontal" role="form">';
        form += date + charDate + image + save;
        $.window(form, {
            title: 'Plano de fundo',
            width: 400
        });
        $.fields.executeAndclearPos();
    }

    /**
     * Botão para o background
     * @returns {undefined}
     */
    this.triggerNovoBack = function() {
        $.FormBack.list();
    }

    this.populateBack = function() {
        var backObj = new ModelBack();
        var arrList = backObj.listar(true);
        $.Populate.tbodyEditer($("#tbl-back").find('tbody'), arrList, ['data', 'dia', 'image'], 'id_back');

    }

    this.triggerBiblia = function() {
        $('#bible-versions').change(function() {
            var configObj = new ModelConfig();
            configObj.setConfig('bible_version', $(this).find('option:selected').val());
        })
    }

    this.triggerEffectHide = function() {
        var configObj = new ModelConfig();
        var currentEffect = configObj.getConfig('effect-hide')
        $('#dshow-effects').find('option[value="'+currentEffect+'"]').attr('selected',true);
        $('#dshow-effects').change(function() {
            var effect = $(this).find('option:selected').val();
            configObj.setConfig('effect-hide', $(this).find('option:selected').val());
            $("#text-example-back").show();
            setTimeout(function(){
                $("#text-example-back").hide(effect,{},1000);
            },1000)
        })
    }

    this.main = function() {
        this.loadVersionsBible();
        this.triggerNovoBack();
        this.triggerEffectHide();
    }

}