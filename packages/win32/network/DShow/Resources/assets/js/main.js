//inicia a janela
var windowCurrent = Ti.UI.currentWindow;
windowCurrent.maximize();
var fullWindow = windowCurrent.createWindow("app://full.html");
fullWindow.open();

$(document).ready(function() {

    setTimeout(function() {
        $.Full.readFull = fullWindow.readFull;
    },500);
    //Inicia a bíblia
    var bibliaObj = new ViewBiblia();
    bibliaObj.main();
    $('#btn-config').click(function() {
        var currentWindow = Ti.UI.currentWindow;
        var newWindow = currentWindow.createWindow("app://config.html");
        newWindow.setWidth(screen.width - ((screen.width / 10) * 2));
        newWindow.open();
    })

    $('#freeze').click(function() {
        $.freezer.start();
    })
});
