<?php

class Db extends SQLite3 {

    public function __construct() {
        $this->open(realpath(dirname(__FILE__) . '/../') . '/app.db');
    }

    /**
     * Remove os acentos
     * @param string $texto
     * @return type
     */
    public static function retiraAcentos($texto) {
        $array1 = array("á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç"
            , "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç");
        $array2 = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c"
            , "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C");
        $texto = str_replace($array1, $array2, $texto);
        $texto = preg_replace("/[^a-z0-9\s\-]/i", "", $texto);
        return $texto;
    }

    public function __destruct() {
        $this->close();
    }

}
