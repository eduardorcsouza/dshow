$.fields = {
    _fnPos: {},
    text: function(name, label) {
        return Template.fetch('fields/text.html', {
            name: name,
            label: label
        });
    },
    savebtn: function(name, label) {
        return Template.fetch('fields/savebtn.html', {
            name: name,
            label: label
        });
    },
    date: function(name, label) {
        this._fnPos['dt-' + name] = function() {
            $('input[name="' + name + '"]').datepicker();
        }
        return Template.fetch('fields/date.html', {
            name: name,
            label: label
        });
    },
    image: function(name, label) {
        this._fnPos['fli-' + name] = function() {
            $('#file-' + name).click(function() {
                Ti.UI.openFileChooserDialog(function(filenames) {
                    $('input[name="' + name + '"]').val(filenames[0]);
                    $('#image-'+name).show().find("img").attr("src",'file:///'+filenames[0]);
                }, {
                    multiple: false,
                    title: "Selecione uma imagem",
                    types: ['jpg','png', 'gif'],
                    typesDescription: "Imagens",
                    path: Ti.Filesystem.getUserDirectory()
                })
            });
        }
        return Template.fetch('fields/image.html', {
            name: name,
            label: label
        });
    },
    //executa dos dados apos ser renderizados
    executePos: function() {
        var i;
        for (i in this._fnPos) {
            this._fnPos[i]();
        }
    },
    //limpas as funcoes
    clearPos: function() {
        this._fnPos = {};
    },
    //executa e limpas as funcoes
    executeAndclearPos: function() {
        this.executePos();
        this.clearPos();
    }
}

