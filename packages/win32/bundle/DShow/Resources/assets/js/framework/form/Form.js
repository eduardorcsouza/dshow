$.Form = {
    //titulo do form
    title: 'Form',
    //tabela
    table: 'tbl',
    //array de campos
    _arrFields: {},
    //nome e valor da pk
    pkName: 'id',
    pkVal: 0,
    selector: "#lister",
    permision: 3,
    /**
     * Cria a tabela
     * @param {array} arr array de dados
     * @returns {undefined}
     */
    _createTable: function(arr) {
        var table = '';
        var obj = this;
        var i, ii, tr, td, th, tbody, thead;
        thead = $('<thead/>');
        tbody = $('<tbody/>');
        tr = $('<tr/>');
        for (i in this._arrFields) {
            th = $('<th/>').html(this._arrFields[i].label.toUpperCase());
            th.appendTo(tr);
        }
        tr.appendTo(thead);
        for (i in arr) {
            tr = $('<tr/>');
            for (ii in arr[i]) {
                if (ii !== this.pkName) {
                    td = $("<td/>").html(arr[i][ii]);
                    tr.append(td);
                }
            }
            td = $("<td/>").html(this._btns(arr[i][this.pkName]));
            tr.append(td);
            tr.appendTo(tbody);
        }
        $(this.selector).html('');
        $('<button/>').addClass('btn btn-success insert').html('Adicionar').appendTo(this.selector)
        var table = $('<table/>').addClass('table-responsive').attr("width", '100%')
                .append(thead)
                .append(tbody)
                .appendTo(this.selector);
        $('.delete', $(this.selector)).click(function() {
            if (confirm('Você deseja deletar esse registro?')) {
                var rel = $(this).attr('rel');
                obj.pkVal = rel;
                obj.del();
                obj.list();
            }
        });
        $('.edit', $(this.selector)).click(function() {
            var rel = $(this).attr('rel');
            obj.pkVal = rel;
            obj._arrFields = {};

            obj.config();
            var sql = obj.sqlList();
            obj.setResultData(databaseObj.execute(sql));

            obj._createForm();
        });
        $('.insert', $(this.selector)).click(function() {
            obj.pkVal = 0;
            obj._arrFields = {};
            obj.config();
            obj._createForm();
        });

    },
    _createForm: function() {
        var obj = this;
        var form = $("<form/>");
        $(form).attr("role", 'form').addClass('form-horizontal');
        $('<input/>').attr({
            'type': 'hidden',
            'name': this.pkName,
            'value': this.pkVal
        });
        var i;
        for (i in this._arrFields) {
            $(form).append(this._arrFields[i].getForm());
        }
        $(form).append(this._btnSave())
        var div = $('<div/>');
        $(div).append(form);
        var janela = $.window($(div).html(), {
            width: 500
        });
        for (i in this._arrFields) {
            this._arrFields[i].posForm();
        }
        $('#btn-save', janela).click(function() {
            obj.setSerialized($('form', janela).serializeArray());
            obj.save();
            $(this).parents('.ui-dialog').find('.ui-dialog-titlebar-close').trigger('click');
            obj.list();
        });

    },
    _btnSave: function() {
        return Template.fetch('fields/savebtn.html', {
            name: 'save',
            label: 'Salvar'
        });
    },
    _btns: function(id) {
        var btns = '';
        id = (id != undefined) ? "rel='" + id + "'" : '';
        if (this.permision > 0) {
            btns += "<button " + id + " class='btn btn-default edit'>Editar</button>";
        }
        if (this.permision > 1) {
            btns += "<button " + id + " class='btn btn-danger delete'>Deletar</button>";
        }
        return btns;
    },
    /**
     * Adiciona um campo
     * @param {Field} field objeto do campo
     * @returns {undefined}
     */
    addField: function(field) {
        field.setPk(this.pkName, this.pkVal);
        field.setTable(this.table);
        this._arrFields[field.name] = field;
    },
    /**
     * SELECT para a listagem
     * @returns {String}
     */
    sqlList: function() {
        var sql = "SELECT * FROM " + this.table;
        if (this.pkVal) {
            sql += " WHERE " + this.pkName + "=" + this.pkVal;
        }
        return sql;
    },
    config: function() {

    },
    /**
     * Seta dados para listar e torna o TD
     * @param {resultset} resultObj
     * @returns {array}
     */
    setResultData: function(resultObj) {
        var pkVal = resultObj.fieldByName(this.pkName);
        var fieldVal, i;
        var arr = {};
        for (i in this._arrFields) {
            this._arrFields[i].setPk(this.pkName, pkVal);
            fieldVal = resultObj.fieldByName(this._arrFields[i].name);
            if (fieldVal !== undefined && fieldVal !== null) {
                this._arrFields[i].val = fieldVal;
            }
            arr[this._arrFields[i].name] = this._arrFields[i].getValList();
        }
        arr[this.pkName] = pkVal;
        return arr;
    },
    /**
     * Seta dados para listar e torna o TD
     * @param {Object} resultObj
     * @returns {array}
     */
    setSerialized: function(serial) {
        var pkVal = this._arrFields[this.pkName];
        var field, i;
        for (i = 0; i < serial.length; i++) {
            field = serial[i];
            if (field.name === this.pkName) {
                this.pkVal = field.value;
            }
        }
        for (i = 0; i < serial.length; i++) {
            field = serial[i];
            if (this._arrFields[field.name] !== undefined) {
                this._arrFields[field.name].setPk(this.pkName, this.pkVal);
                if (field.value !== undefined && field.value !== null) {
                    this._arrFields[field.name].setVal(field.value);
                }
            }
        }
    },
    /**
     * Faz um select do banco
     * @returns {undefined}
     */
    list: function() {
        this.pkVal = 0;
        this._arrFields = {};
        this.config();
        var resultObj = databaseObj.execute(this.sqlList());
        var arr = [];
        while (resultObj.isValidRow()) {
            arr.push(this.setResultData(resultObj));
            resultObj.next();
        }
        this._createTable(arr);
    },
    /**
     * Salva os dados no banco
     * @returns {undefined}
     */
    save: function() {
        this.pkVal = parseInt(this.pkVal);
        this.pkVal = isNaN(this.pkVal) ? 0 : this.pkVal;
        var sql, i;
        var len = 0;

        if (this.pkVal <= 0) {
            var arrFields = [];
            var arrVals = [];
            for (i in this._arrFields) {
                arrFields.push(this._arrFields[i].name);
                arrVals.push(this._arrFields[i].getValInsertUpdate());
            }
            sql = "INSERT INTO " + this.table + " ";
            sql += "(" + arrFields.join(',') + ") values";
            sql += "(" + arrVals.join(',') + ")";

        } else {
            var arrUpdate = [];
            for (i in this._arrFields) {
                arrUpdate.push(this._arrFields[i].name + '=' + this._arrFields[i].getValInsertUpdate());
            }
            sql = "UPDATE " + this.table + " SET ";
            sql += arrUpdate.join(',');
            sql += " WHERE " + this.pkName + " = " + this.pkVal;
        }
        databaseObj.execute(sql);
    },
    /**
     * Deleta os dados
     * @returns {undefined}
     */
    del: function() {
        var sql = "DELETE FROM " + this.table + " WHERE " + this.pkName + " = " + this.pkVal;
        databaseObj.execute(sql);
    }

}
$.Form.fields = {};
$.Form.fields.base = {
    name: '',
    label: '',
    val: '',
    pkName: '',
    pkVal: 0,
    table: '',
    _addslashes: function() {
        return this.val.replace("'", "\'");
    },
    getValList: function() {
        return this.val;
    },
    getForm: function() {
        return this.val;
    },
    getValInsertUpdate: function() {
        return "'" + this._addslashes() + "'";
    },
    setVal: function(val) {
        this.val = val;
    },
    setTable: function(table) {
        this.table = table
    },
    setPk: function(name, val) {
        this.pkName = name;
        this.pkVal;
    },
    posForm: function() {

    },
    posList: function() {

    }
}

$.Form.fields.text = $.extend(cloneObj($.Form.fields.base), {
    getForm: function() {
        return Template.fetch('fields/text.html', {
            name: this.name,
            label: this.label,
            val: this.val
        });
    }
})
$.Form.fields.date = $.extend(cloneObj($.Form.fields.base), {
    getForm: function() {
        return Template.fetch('fields/date.html', {
            name: this.name,
            label: this.label,
            val: this.val
        });
    },
    posForm: function() {
        $('#field-date-' + this.name).datepicker();
    }
})
$.Form.fields.image = $.extend(cloneObj($.Form.fields.base), {
    getForm: function() {
        return Template.fetch('fields/image.html', {
            name: this.name,
            label: this.label,
            val: this.val
        });
    },
    getValList: function() {
        if(this.val !== ""){
            var img = "<img style='width:150px' src='"+this.val+"' ./>";
        }
        return img;
    },
    posForm: function() {
        var obj = this;
        $('#file-' + this.name).click(function() {
            Ti.UI.openFileChooserDialog(function(filenames) {
                 var ext = end(filenames[0].split('.'));
                 console.log(filenames[0]);
                 var file = Ti.Filesystem.getFile(filenames[0]);
                 var newFileName = 'files/'+generatePassword(16)+'.'+ext;
                 var newFile = Ti.Filesystem.getFile(
                         Ti.Filesystem.getResourcesDirectory()+'/'+newFileName
                 );
                 if(file.exists()){
                     newFile.write(file.read());
                 }
                $('input[name="' + obj.name+'"]').val(newFileName);
                $('#image-' + obj.name).show().find("img").attr("src", newFileName);
            }, {
                multiple: false,
                title: "Selecione uma imagem",
                types: ['jpg', 'png', 'gif'],
                typesDescription: "Imagens",
                path: Ti.Filesystem.getUserDirectory()
            });
        });
        if($('#image-' + this.name).find("img").attr("src")!== ""){
            $('#image-' + this.name).show()
        }
    }
})