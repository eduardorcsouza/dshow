$.window = function(content,opt){
    
    var param = $.extend({
        'title':'',
        width:'auto',
        height: 'auto',
        close: function(){
            $(this).remove();
        }
    },opt)
    
    var div = $('<div/>');
    $(div).html(content).appendTo('body');
    $(div).dialog(param);
    return div;
}