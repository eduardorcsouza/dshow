$.freezer = {
    fn: [],
    isFreezer: false,
    add: function(fn) {
        this.fn.push(fn);
    },
    start: function() {
        this.isFreezer = (!this.isFreezer);
        for (var i = 0; i < this.fn.length; i++) {
            this.fn[i](this.isFreezer);
        }
    }
}