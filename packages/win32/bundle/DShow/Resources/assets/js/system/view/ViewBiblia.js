$.biblia = {
    idAtual: 0
}
function ViewBiblia() {

    var obj = this;

    /**
     * Chama um php que faz a busca
     * @returns {undefined}
     */
    var buscar = function() {
        var bibliaObj = new ModelBiblia();
        var busca = $('#busca-biblia-full').val();
        $('#biblia-busca-resultado').html('').show();
        $('#biblia-versiculo').hide();

        var arr = bibliaObj.buscar(busca);
        var ul = $('<ul/>');
        $('#biblia-busca-resultado').html('').append(ul);
        $.Populate.lister(ul, arr, 'title', 'desc', 'id_versiculo');
    }

    /**
     * Preenche o combo com os livros
     * @returns {undefined}
     */
    var preencheLivro = function() {
        var bibliaObj = new ModelBiblia();
        var arrLivros = bibliaObj.getLivros();
        $.Populate.combo('#busca-livro', arrLivros, 'id', 'nome');
    }
    /**
     * Retorna o Versiculo pela referencia
     * @returns {undefined}
     */
    var referenciaVersiculo = function() {
        var bibliaObj = new ModelBiblia();

        //recupera os daddos de livro capitulo e versículo
        var livro = $('#busca-livro').find('option:selected').val();
        var capitulo = $('#busca-capitulo').val();
        var versiculo = $('#busca-versiculo').val();

        var arrBible = bibliaObj.getEscrituraByReferencia(livro, capitulo, versiculo);
        showVersiculo(arrBible);
    }

    /**
     * Retorna o versiculo pelo ID
     * @param {int} id
     * @returns {undefined}
     */
    var getIdVersiculo = function(id) {
        if (id > 0) {
            var bibliaObj = new ModelBiblia();
            var arrBible = bibliaObj.getEscrituraByID(id);
            showVersiculo(arrBible);
        }
    }

    /**
     * Próximo versiculo
     * @returns {undefined}
     */
    this.next = function() {
        $.biblia.idAtual++;
        getIdVersiculo($.biblia.idAtual);
    }

    /**
     * Versiulo anterior
     * @returns {undefined}
     */
    this.prev = function() {
        $.biblia.idAtual--;
        getIdVersiculo($.biblia.idAtual);
    }

    /**
     * Versiulo anterior
     * @returns {undefined}
     */
    var current = function() {
        getIdVersiculo($.biblia.idAtual);
    }

    /**
     * Mostra o versiculo
     * @param {type} arrBible
     * @returns {undefined}
     */
    var showVersiculo = function(arrBible) {
        $.biblia.idAtual = arrBible.id;
        var blb = $('#biblia-versiculo');
        blb.show();
        $('.escritura-content', blb).show();
        $('.livro', blb).html(arrBible.livro);
        $('.capitulo', blb).html(arrBible.capitulo);
        $('.versiculo', blb).html(arrBible.versiculo);
        $('.escritura', blb).html(arrBible.texto);
        var data = Template.fetch('bible.html', arrBible);
        if ($.arrConfig.show) {
            $.Full.readFull(data);
        }
    }

    var triggerBuscarReferencia = function() {
        $('#biblia-select').find(".dshow-btn").click(function() {
            referenciaVersiculo();
        })
    }

    /**
     * Trigger para botão next e prev
     * @returns {undefined}
     */
    var triggerNextPrev = function() {
        $("#bible-next").click(function() {
            obj.next();
        })
        $("#bible-prev").click(function() {
            obj.prev();
        })
    }

    /**
     * Mostra o versiculo
     * @returns {undefined}
     */
    var triggerMostrarVersiculo = function() {
        $("#show-versiculo").click(function() {
            $.arrConfig.show = true;
            if ($.biblia.idAtual > 0) {
                current();
            }
        })
    }

    /**
     * Trigger que mostra o plano de fundo
     * @returns {undefined}
     */
    var triggerMostrarFundo = function() {
        $('#show-back').click(function() {
            $.Full.readFull('hider');
            $.arrConfig.show = false;
        })
    }

    /**
     * Popula as versões da bíblia
     * @returns {undefined}
     */
    var populateVersions = function() {
        var bibleObj = new ModelBiblia();
        var arrBible = bibleObj.bibleVersions();
        $.Populate.ul($('ul.bible-versions'), arrBible, 'id', 'name');
        selectVersion();
    }

    /**
     * Seleciona uma versão
     * @returns {undefined}
     */
    var selectVersion = function() {
        var config = new ModelConfig();
        var version = config.getConfig('bible_version');
        $('ul.bible-versions').find('li[rel="' + version + '"]').addClass('active');
        $('ul.bible-versions').find('li').click(function() {
            var rel = $(this).attr("rel");
            config.setConfig('bible_version', rel);
            $('ul.bible-versions').find('li').removeClass('active');
            $(this).addClass('active');
            current();
            loadSaves();
        })
    }

    /**
     * Carrega os versiculos salvos
     * @returns {undefined}
     */
    var loadSaves = function() {
        var bibliaObj = new ModelBiblia();
        var arr = bibliaObj.listSalvos();
        $.Populate.ul('#bible-salvos', arr, 'id_salvo', 'desc');
        $('#bible-salvos').find('li').click(function() {
            var arrBible = bibliaObj.bibleLoadBySave($(this).attr('rel'));
            showVersiculo(arrBible);
        })
    }

    /**
     * Salva o versículo
     * @returns {undefined}
     */
    var triggerSaveVersiculo = function() {
        var bibliaObj = new ModelBiblia();
        $('#save-versiculo').click(function() {
            if ($.biblia.idAtual > 0) {
                bibliaObj.bibleSaveVersiculo($.biblia.idAtual);
                loadSaves();
            }
        });
    }

    /**
     * Trigger do botão buscar
     * @returns {undefined}
     */
    var triggerBusca = function() {
        $('#btn-busca').click(function() {
            buscar();
            $('#biblia-busca-resultado').find('li').click(function() {
                var rel = $(this).attr('rel');
                getIdVersiculo(rel);
                $('#biblia-busca-resultado').hide();
            })
        })
    }

    var addBibleFreezer = function() {
        $.freezer.add(function(freezer) {
                $.arrConfig.show = freezer;
            });
    }

    this.main = function() {
        triggerMostrarVersiculo();
        triggerBuscarReferencia();
        triggerMostrarFundo();
        triggerNextPrev();
        preencheLivro();
        populateVersions();
        triggerSaveVersiculo();
        loadSaves();
        triggerBusca();
        addBibleFreezer();
    }
}