$.fn.tab = function() {

    var lisObj = $('li', this);
    var liObj, item;
    var ativo = false;
    for (var i = 0; i < lisObj.length; i++) {
        liObj = lisObj.eq(i);
        item = liObj.attr("rel");
        if (item !== undefined) {
            $(item).hide();
            if (liObj.hasClass('active')) {
                ativo = item;
            }
        }
    }
    if (ativo) {
        $(ativo).show();
    } else {
        lisObj.eq(0).addClass('active');
        item = lisObj.eq(0).attr("rel");
        if (item !== undefined) {
            $(ativo).show(item);
        }
    }
    $(lisObj).click(function() {
        var item, liObj;
        for (var i = 0; i < lisObj.length; i++) {
            liObj = lisObj.eq(i);
            $(liObj).removeClass('active');
            item = liObj.attr("rel");
            if (item !== undefined) {
                $(item).hide();
            }
        }
        $(this).addClass('active')
        item = $(this).attr('rel');
        if (item !== undefined) {
            $(item).show();
        }
    })

}